import React from 'react';
import './TableStyle.css';

/*
  TABLE - This component is a dynamically created table. It created a table
  via a json file, pass into it's props.

  Example JSON File:
  {
    "model": "hr.employee",
    "pk": 1,
    "fields": {
      "first_name": "John",
      "last_name": "Doe",
      "student_number": 12345,
      "global_id": "doe1j",
    }
  }, {
    "model": "hr.employee",
    "pk": 2,
    "fields": {
      "first_name": "Jane",
      "last_name": "Doe",
      "student_number": 54321,
      "global_id": "doe2j",
    }
  }

  Props:
    - [JSON] data: The data as a json object
    - [ARRAY] headers_json: The table headers (as found in json object)
      -Ex: ["first_name", "last_name"]
    - [ARRAY] headers_text: The table headers to display to the user
      -Ex: ["First Name", "Last Name"]
    - [BOOLEAN] interactive: Whether the table should be interactive (have links to edit rows)
    - [STRING] interactiveText: The text that should display in the interactive column as the link text
      -Ex: "Edit"
    - [FUNCTION] onInteract: The function that should be called when the interact text is clicked
*/
export default function Table(props) {
  const tableHeaders = props.headers_json.map((header, index) => <th key={header} className="table-tableHeader">{props.headers_text[index]}</th>)

  const formatNumber = (num) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  const RenderRow = (props) => {
    return props.headers.map((header, index) => {
        let data = props.data[header];
        if(!isNaN(data) && !isNull(data)) {
          data = data.toFixed(2);
          data = formatNumber(data);
          data += "gha";
        }
        return <td key={`${header}-${index}`} className="table-td">{isNull(props.data[header]) ? "No Data Available" : data.toString()}</td>
    });
  }

  const tableCells = props.data.map((element, index) => {
    return <tr key={index} className="table-tr">
              {props.interactive === true ? <td key={`interactive-${index}`} className="table-td">
                <span className="table-link" onClick={props.onInteract ? props.onInteract : () => {}}>{props.interactiveText}</span>
              </td> : ""}
              <RenderRow key={index} data={element} headers={props.headers_json}/>
           </tr>
  });

  const isNull = (object) => {
    if (object === null || object === undefined || object === "")
      return true;
    else
      return false;
  }

  return (
    <>
      <table id={props.id} className="table-table">
        <thead className="table-thead">
          <tr className="table-tr">
            {props.interactive ? <th key="interactive" className="table-tableHeader"></th> : ""}
            {tableHeaders}
          </tr>
        </thead>
        <tbody className="table-tbody">
          {tableCells}
        </tbody>
      </table>
    </>
  );
}

Table.defaultProps = {
  interactive: false,
  interactiveText: "Edit",
  headers_json: ["None"],
  headers_text: ["None"],
}
