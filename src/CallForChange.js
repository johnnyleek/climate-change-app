import React from 'react';

export default function CallForChangeView() {
  return (
    <div id="page-content">
      <h1 id="page-header">How to Support the Climate Change Movement</h1>
      <p>
        In todays society (particularly in America), the Climate Change Movement
        needs a lot of support. From skeptics that don't believe that the issue
        is real, to lobbyists paying endless amounts of money to supress the
        movement, there is a lot of work to be done and ground to be covered
        in order to make an impact. Because of this, there are many things that
        you can do to try and make that impact.
      </p>
      <p>
        The first thing you can do is make the difference yourself. Take it upon
        yourself to reduce the amount of waste you produce, and limit your
        carbon footprint. For a few ways to do that, check out:
        &nbsp;<a href="/minimize_usage" className="inline-link">"How to: Minimize Usage"</a>
      </p>
      <p>
        Another way to make an impact is to contact the representatives in your
        area and let them know that you care about this cause. To find out
        the representatives in your area (US States only), go to&nbsp;
        <a href="https://www.house.gov/representatives/find-your-representative" target="_blank" rel="noopener noreferrer" title="US Government 'Find Your Representative'" className="inline-link">this link</a>,
        and type in your zip code to find your local representative.
      </p>
    </div>
  );
}
