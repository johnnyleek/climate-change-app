import React, { useState, useEffect } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

import Table from './Table';
import LoadingIcon from './LoadingIcon';

export default function CountryView() {
  const [countryData, setCountryData] = useState();
  const [countryInfo, setCountryInfo] = useState([]);
  const [isPerCapita, setIsPerCapita] = useState(false);
  const [chartLoading, setChartLoading] = useState(true);

  const fetch = require('node-fetch');
  const https = require('https');

  const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
  });

  const getMapColor = value => {
    let colors = {
      red: "#e74c3c",
      yellow: "#f39c12",
      light_blue: "#3498db",
      green: "#00bc8c"
    }

    if(!isPerCapita) {
      if(value >= 1000000000) {
        return colors.red;
      }
      else if(value >= 250000000) {
        return colors.yellow;
      }
      else if(value >= 100000000) {
        return colors.light_blue;
      }
      else {
        return colors.green;
      }
    } else {
      if(value >= 6) {
        return colors.red;
      }
      else if(value >= 4) {
        return colors.yellow;
      }
      else if(value >= 2) {
        return colors.light_blue;
      }
      else {
        return colors.green;
      }
    }
  };

  useEffect(() => {
    setChartLoading(true);
    let map = am4core.create("country_chart", am4maps.MapChart);
    map.geodata = am4geodata_worldLow;
    map.projection = new am4maps.projections.Miller();
    map.numberFormatter.numberFormat = "#,###.## gha";

    let mapGeoData = map.series.push(new am4maps.MapPolygonSeries());
    mapGeoData.useGeodata = true;
    mapGeoData.exclude = ["AQ"];

    let mapTemplate = mapGeoData.mapPolygons.template;
    mapTemplate.tooltipText = "{name}: {value}";
    mapTemplate.fill = am4core.color("#333");

    let hoverState = mapTemplate.states.create("hover");
    hoverState.properties.fill = am4core.color("#367B25");

    //Get data from footprintnetwork API
    fetch(`https://api.footprintnetwork.org/v1/data/all/2016/EFC${isPerCapita ? "pc" : "tot"}`, {
      headers: {
        Accept: "application/json",
        Authorization: "Basic Sm9obm55TGVlazpmQ1VPbUEzYTFKZjMxNDg1M0doVmMzYjNjdUdyUUoxTHQwNU0xcW42Qkw1YkIwczUwM0c="
      },
      agent: httpsAgent,
    })
    .then(response => response.json())
    .then(data => {setCountryData(data); return data})
    .then(data => {
      setCountryInfo([]);
      for(let country in Object.keys(data)) {
        countryInfo.push({ "id": data[country]["isoa2"], "name": data[country]["countryName"], "value": data[country]["value"], "fill": am4core.color(getMapColor(data[country]["value"])) })
      }
      mapGeoData.data = countryInfo;
      mapTemplate.propertyFields.fill = "fill";
      setChartLoading(false);
    })
    .catch(error => console.log(error));
  }, [isPerCapita]);

  return (
    <div id="page-content">
      <h1 id="page-header">Ecological Footprint in Global Hectares {isPerCapita ? "(Per capita)" : ""}</h1>
      <div id="capita-container">
        <ul id="capita-checkbox">
          <li>
            <input type="checkbox" value="perCapita" id="perCapitaBox" onClick={() => setIsPerCapita(!isPerCapita)}/>
            <label htmlFor="perCapitaBox">Per Capita?</label>
          </li>
        </ul>
      </div>
      <div id="country_chart" className={`${chartLoading ? "loading" : ""}`}>
      </div>
      {chartLoading ? <LoadingIcon className="ecoFootprintLoading"/> : ""}


      <div id="country_table">
      {countryData !== undefined
        ?
         <>
         <Table
            data={countryData}
            headers_json={["countryName", "builtupLand", "carbon", "cropLand", "fishingGround", "forestLand", "grazingLand"]}
            headers_text={["Country", "Built Up Land", "Carbon Footprint", "Cropland", "Fishing Ground", "Forest Land", "Grazing Land"]}/>
         </>
        :
          <p></p>
        }
      </div>
    </div>
  );
}
