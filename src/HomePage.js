import React from 'react';

export default function HomePage(props) {

  return (
    <div id="page-content">
      <h1 id="page-header">Welcome to the Climate Change Visualizer!</h1>

      <p>
        This app was created over a short period of time with the intent of mapping
        different aspects of Climate Change around the world. The two current available
        maps are
      </p>
      <ol>
        <li><a className="inline-link" href="/ecological_footprint">Ecological Footprint by Country</a></li>
        <li><a className="inline-link" href="/biocapacity">Biocapacity by Country</a></li>
      </ol>
      <p>
        These can also be toggled to view per capita data as well (to get a more
        complete picture of what is happening).
      </p>
      <p>
        You can also view some tips on reducing your carbon footprint, and ways
        to support the Climate Change movement!
      </p>
      <hr/>
      <p>
        I hope you enjoy! If you like what you see and want to see some other
        things, you can view my links below:
      </p>
      <ul>
        <li><a className="inline-link" href="https://gitlab.com/johnnyleek">GitLab (view the code of my passion projects)</a></li>
        <li><a className="inline-link" href="https://www.linkedin.com/in/johnny-leek-3551191a7/">LinkedIn (See my professional experience)</a></li>
      </ul>
    </div>
  );

}
