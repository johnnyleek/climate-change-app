import React from 'react';

export default function MinimizeUsageView() {

  return (
    <div id="page-content">
      <h1 id="page-header">How to Minimize your Carbon Footprint.</h1>

      <p>
        The average carbon footprint of an individual on Earth
        is ~4 tons. However, in the United States, this average is
        closer to 16 tons<a className="inline-link" href="https://www.nature.org/en-us/get-involved/how-to-help/carbon-footprint-calculator/" target="_blank" rel="noopener noreferrer"><sup>1</sup></a>.
        There are many easy things you can do each and every day in order
        to bring this usage down, in turn bringing down your carbon
        footprint. Here are some things you can do in different aspects
        of your life (some easier than others!)
      </p>
      <p>
        Transport:
      </p>
      <ul>
        <li>
          Drive a more efficient vehicle
        </li>
        <li>
          Take public transportation
        </li>
        <li>
          Reduce air travel
        </li>
        <li>
          Carpool to work and school
        </li>
      </ul>
      <p>
        Housing:
      </p>
      <ul>
        <li>
          Switch from standard bulbs to LEDs
        </li>
        <li>
          Reduce thermostat temperature in winter. Increase in summer.
        </li>
        <li>
          Plant trees
        </li>
        <li>
          Install water-efficient faucets, showerheads, toilets, etc.
        </li>
      </ul>
      <p>
        Shopping / Diet:
      </p>
      <ul>
        <li>
          Buy locally grown/produced foods
        </li>
        <li>
          Eat organic produce
        </li>
        <li>
          Cut down on meat consumption
        </li>
        <li>
          Use reuseable bags instead of plastic bags
        </li>
      </ul>
      <p>
        These are just a few very simple ways to reduce your carbon footprint.
        If everybody did just a few of these things, the global footprint would
        be reduced drastically.
      </p>
    </div>
  );

}
