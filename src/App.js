import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

import HomePage from './HomePage';
import CountryView from './CountryView';
import BiocapacityView from './BiocapacityView';
import MinimizeUsageView from './MinimizeUsageView';
import CallForChangeView from './CallForChange';

function App() {
  return (
    <Router>
      <nav className="navbar">
        <ul className="navbar-nav">
          <li class="nav-brand">
            <NavLink to="/" className="nav-link">
              <span className="material-icons nav_logo">home</span>
              <span className="link-text brand-text">CLIMATE APP</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/ecological_footprint" className="nav-link" activeClassName="nav-active">
              <span className="material-icons">public</span>
              <span className="link-text">Ecological Footprint by Country</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/biocapacity" className="nav-link" activeClassName="nav-active">
              <span className="material-icons">nature</span>
              <span className="link-text">Biocapacity by Country</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/minimize_usage" className="nav-link" activeClassName="nav-active">
              <span className="material-icons">person</span>
              <span className="link-text">How to: Minimize Usage</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/support_cause" className="nav-link" activeClassName="nav-active">
              <span className="material-icons">gavel</span>
              <span className="link-text">How to: Support the Movement</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <a href="https://gitlab.com/johnnyleek" target="_blank" rel="noopener noreferrer" className="nav-link">
              <span className="material-icons">sentiment_satisfied</span>
              <span className="link-text">Made with love by Johnny Leek</span>
            </a>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route path="/ecological_footprint">
          <CountryView/>
        </Route>
        <Route path="/biocapacity">
          <BiocapacityView/>
        </Route>
        <Route path="/minimize_usage">
          <MinimizeUsageView/>
        </Route>
        <Route path="/support_cause">
          <CallForChangeView/>
        </Route>
        <Route exact path="/">
          <HomePage/>
        </Route>
        <Route path="/">
          <h1>404. Page not found</h1>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
